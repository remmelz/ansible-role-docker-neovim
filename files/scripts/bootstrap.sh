#!/bin/bash

###################
apt-get -y update && \
apt-get -y install \
  xz-utils \
  wget \
  tree \
  tmux \
  git \
  tig && \
###################
useradd -m -s /bin/bash neovim && \
su - neovim -c 'git clone https://gitlab.com/remmelz/neovim-configs.git' && \
su - neovim -c 'cd neovim-configs/ && ./install.sh -y'

