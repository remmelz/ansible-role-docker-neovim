#!/bin/bash

volume=$1

if [[ -z ${volume} ]]; then
  echo 'Error: no shared volume selected.'
  echo
  docker volume ls
  exit 1
fi

docker run --rm -it \
  --volume ide_consol_neovimc:/home/neovim \
  --volume ${volume}:/share \
  ide_consol_neovimc \
  su - neovim


